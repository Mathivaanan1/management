import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit{
  constructor(private https:HttpClient){}
  ngOnInit(): void {}
 todo:any;

  onCreatePost(postData: { firstname: string; lastname: string;date: string;age: string;address: string }) {
    // Send Http request
    this.https.post('https://customerhttp-default-rtdb.asia-southeast1.firebasedatabase.app/customer.json',postData)
    .subscribe(responseData => {
      console.table(responseData);
    });
}

onShow() {
  this.https
    .get('https://customerhttp-default-rtdb.asia-southeast1.firebasedatabase.app/customer.json')
    .subscribe((data: any) => {
      console.table(data);
      this.todo = data;
    });
}
onDelete(){

  // Send Http request
  this.https
    .delete(
      'https://customerhttp-default-rtdb.asia-southeast1.firebasedatabase.app/customer.json',
      // deleteData
    )
    .subscribe((responseData) => {
      console.log(responseData);
    });

}



 date=Date()

  curDate:number=2023
 
  result:any

 convert(){

 this.date= this.date.slice(0,4)

 this.result=this.curDate-Number(this.date)

}
dialogbox(){
  confirm("Login Success");
}

}
