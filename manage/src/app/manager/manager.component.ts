import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.css']
})
export class ManagerComponent implements OnInit {
todo:any;
tata:any;
  constructor(private https:HttpClient){}
  ngOnInit(): void {}
  onCreatePost(postData: { firstname: string; lastname: string;date: string;age: string;about: string }) {
    // Send Http request
    this.https.post('https://mathihttp-default-rtdb.asia-southeast1.firebasedatabase.app/manager.json',postData)
    .subscribe(responseData => {
      console.table(responseData);
    });
}
onShow() {
  this.https
    .get('https://mathihttp-default-rtdb.asia-southeast1.firebasedatabase.app/manager.json')
    .subscribe((data: any) => {
      console.table(data);
      this.todo = data;
    });
}
role(){
  this.https.get('https://mathihttp-default-rtdb.asia-southeast1.firebasedatabase.app/manager.json')
  .subscribe((data:any)=>{
    console.table(data)
    this.tata=data;
  });
}
onDelete(){

  // Send Http request
  this.https
    .delete(
      'https://mathihttp-default-rtdb.asia-southeast1.firebasedatabase.app/manager.json',
      // deleteData
    )
    .subscribe((responseData) => {
      console.log(responseData);
    });

}

date=Date()

  curDate:number=2023
 
  result:any

 convert(){

 this.date= this.date.slice(0,4)

 this.result=this.curDate-Number(this.date)

}
dialogbox(){
  confirm("Login Success");
}

}
