import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  constructor(private https:HttpClient){}
  ngOnInit(): void { }
  todo:any;
  formatLabel(value: number): string {
    if (value >= 1000) {
      return Math.round(value / 1000)+'year';
    }

    return `${value}`;
  }

  onCreatePost(postData: { firstname: string; lastname: string;date: string;age: string;exp: string;male:string;female:string }) {
    // Send Http request
    this.https.post('https://employeehttp-default-rtdb.asia-southeast1.firebasedatabase.app/employee.json',postData)
    .subscribe(responseData => {
      console.table(responseData);
    });
}
onShow() {
  this.https
    .get('https://employeehttp-default-rtdb.asia-southeast1.firebasedatabase.app/employee.json')
    .subscribe((data: any) => {
      console.table(data);
      this.todo = data;
    });
}
onDelete(){

  // Send Http request
  this.https
    .delete(
      'https://employeehttp-default-rtdb.asia-southeast1.firebasedatabase.app/employee.json',
      // deleteData
    )
    .subscribe((responseData) => {
      console.log(responseData);
    });

}











date=Date()

  curDate:number=2023
 
  result:any

 convert(){

 this.date= this.date.slice(0,4)

 this.result=this.curDate-Number(this.date)

}
dialogbox(){
  confirm("Login Success");
}

}
