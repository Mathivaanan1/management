import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManagerComponent } from './manager/manager.component';
import { EmployeeComponent } from './employee/employee.component';
import { CustomerComponent } from './customer/customer.component';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { Main1Component } from './main1/main1.component';


const routes: Routes = [
 {path:'app',component:AppComponent},
{path:'login',component:LoginComponent},
  {path:'manager',component:ManagerComponent},
  {path:'employee',component:EmployeeComponent},
  {path:'customer',component:CustomerComponent},
  {path:'main1',component:Main1Component}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
